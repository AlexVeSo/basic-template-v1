# README #

Joomla 3 template powered by bootstrap 3/4. 

### What is this repository for? ###

* This is a template that is being build for Joomla 3.x.x, that will support bootstrap 3/4
* Version 0.0.1 (Indevelopment)

### What needs to be done ###

* Bootstrap support.
* Font-Awesome support.
* Administration functionality to choose different setting like import boostrap and other frameworks.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* AlexVeSo (alex.vejlgaard@gmail.com)