<?php

define('_JEXEC', 1);
define( 'JPATH_BASE', realpath(dirname(__FILE__).'/../../..' ));
//var_dump( str_replace("\\",'/',JPATH_BASE . '/includes/defines.php'));
require_once str_replace('\\','/',JPATH_BASE) . '/includes/defines.php';
require_once JPATH_BASE . '/includes/framework.php';

// Create the Application
$app = JFactory::getApplication('site');
$doc = JFactory::getDocument();

$params = $app->getTemplate(true)->params;

JResponse::clearHeaders();

header("Content-Type: text/css");
header("X-Content-Type-Options: nosniff");

// Adding main temaplate
echo "@import url('/templates/basic/css/template.css');\n";
//exit;
// Use of Google Font
if ($params->get('googleFont'))
{
    echo "@import url('https://fonts.googleapis.com/css?family={$params->get('googleFontName')}');\n";
	echo "h1, h2, h3, h4, h5, h6, .site-title { font-family: '" . str_replace('+', ' ', $params->get('googleFontName')) . "', sans-serif;}\n";
}

// Template color
if ($params->get('templateColor'))
{
	echo "body.site {
    border-top: 3px solid " . $params->get('templateColor') . ";
    background-color: " . $params->get('templateBackgroundColor') . ";
}
a {
    color: " . $params->get('templateColor') . ";
}
    .nav-list > .active > a,
    .nav-list > .active > a:hover,
    .dropdown-menu li > a:hover,
    .dropdown-menu .active > a,
    .dropdown-menu .active > a:hover,
    .nav-pills > .active > a,
    .nav-pills > .active > a:hover,
    .btn-primary {
    background: " . $params->get('templateColor') . ";
}";
}